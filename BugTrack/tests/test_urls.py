from django.test import SimpleTestCase
from django.urls import reverse, resolve
from BugTrack.views import indexView, loginView, logoutView, forgetPassword, changePassword, registerView, view_Ticket, myTicket, add_Ticket, ticket_details, filterView, viewProfile, editTickets, notAllowedToEdit
class TestUrls(SimpleTestCase):
    
    def test_login_url(self):
        url = reverse('login')
        self.assertEquals(resolve(url).func, loginView)
            
    def test_home_url(self):
        url = reverse('home')
        self.assertEquals(resolve(url).func, indexView)
                    
    def test_logout_url(self):
        url = reverse('logout')
        self.assertEquals(resolve(url).func, logoutView)
                    
    def test_forgetpassword_url(self):
        url = reverse('forgetpassword')
        self.assertEquals(resolve(url).func, forgetPassword)
#How to test those with input?     
    def test_changepassword_url(self):
        url = reverse('changepassword', args=[1])
        self.assertEquals(resolve(url).func, changePassword)
                    
    def test_register_url(self):
        url = reverse('register')
        self.assertEquals(resolve(url).func, registerView)
        
    def test_viewticket_url(self):
        url = reverse('viewticket')
        self.assertEquals(resolve(url).func, view_Ticket)
        
    def test_myTicket_url(self):
        url = reverse('myTicket')
        self.assertEquals(resolve(url).func, myTicket)
        
    def test_addticket_url(self):
        url = reverse('addticket')
        self.assertEquals(resolve(url).func, add_Ticket)
        
    def test_ticketDetails_url(self):
        url = reverse('ticketDetails', args=[1])
        self.assertEquals(resolve(url).func, ticket_details)
        
    def test_filterView_url(self):
        url = reverse('filterView')
        self.assertEquals(resolve(url).func, filterView)
        
    def test_viewProfile_url(self):
        url = reverse('viewProfile')
        self.assertEquals(resolve(url).func, viewProfile)
        
    def test_editTickets_url(self):
        url = reverse('editTickets', args=[1])
        self.assertEquals(resolve(url).func, editTickets)
        
    def test_notAllowedToEdit_url(self):
        url = reverse('notAllowedToEdit')
        self.assertEquals(resolve(url).func, notAllowedToEdit)
        
    