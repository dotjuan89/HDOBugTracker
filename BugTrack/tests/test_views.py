from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User,Group
from BugTrack.models import Skill,UserSkill,Ticket,Comments,Image

class TestViews(TestCase):
    #runs every time before other tests cases
    def setUp(self):
        #setup Client
        self.client = Client()
        
                
        #create user
        self.user = User.objects.create_user(
            username='testuser',
            email='testuser@testemail.test',
            password='testUser@123'
        )
        
        #create ticket
        self.ticket = Ticket.objects.create(
            title='First Ticket',
            description='First Ticket created',
            reportedBy= self.user
        )
        
        #init urls
        self.login_url = reverse('login')
        self.register_url = reverse('register')
        self.home_url = reverse('home')
        self.addticket_url = reverse('addticket')
        self.viewticket_url = reverse('viewticket')
        self.ticketdetails_url = reverse('ticketDetails', kwargs={'pk':self.ticket.pk})
        self.editTickets_url = reverse('editTickets', kwargs={'pk':self.ticket.pk})
        self.viewprofile_url = reverse('viewProfile')
        self.myticket_url = reverse('myTicket')
        self.filterview_url = reverse('filterView')
        
        #create groups
        self.agroup = Group(name='Administrator')
        self.dgroup = Group(name='Developer')
        self.rgroup = Group(name='Reviewer')
        self.tgroup = Group(name='Triager')
        self.bgroup = Group(name='Bug Reporter')
        self.agroup.save()
        self.dgroup.save()
        self.rgroup.save()
        self.tgroup.save()
        self.bgroup.save()
        
        
##################################login test start###############################  
        
    def test_login_GET(self):
        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'login.html')
        
    def test_login_POST(self):
        response = self.client.post(self.login_url, {
            'username':'testuser',
            'password':'testUser@123'
        })
        self.assertEquals(response.status_code,302)
        self.assertIn('_auth_user_id', self.client.session)
        
        
        
        
#################################register test start###################################



    def test_register_GET(self):
        response = self.client.get(self.register_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'register.html')
        
        
        
    def test_register_POST(self):
        response = self.client.post(self.register_url, {
            'username':'newuser',
            'email':'newUser@123.register',
            'password1':'testUser@123',
            'password2':'testUser@123'
        })
        self.assertEquals(response.status_code,302)
        users = User.objects.all()
        self.assertEqual(users.count(), 2)
        
        islogged_in = self.client.login(username='newuser',password='testUser@123')
        self.assertTrue(islogged_in)
        
    
 #############################home test start###############################
    
    
    def test_home_GET(self):
        #no login
        response = self.client.get(self.home_url)
        self.assertNotEquals(response.status_code,200)
        
    def test_home_aGET(self):
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'home.html')
        
        self.user.groups.remove(self.agroup)
        self.client.logout()
        

    def test_home_dGET(self):
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'home.html')        
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
        
    def test_home_tGET(self):
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'home.html')        
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
        
    def test_home_bGET(self):
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'home.html')        
        
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_home_rGET(self):
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'home.html')        
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()
    
###########################addticket test start#################################
    
    def test_add_Ticket_GET(self):
        #no login
        response = self.client.get(self.addticket_url)
        self.assertNotEquals(response.status_code,200)
    
    def test_add_Ticket_POST(self):
        #no login
        self.client.post(self.addticket_url, {
            'title':'addTicket1',
            'severity':1,
            'description':'Testing adding tickets'
        })
        
        ticket = Ticket.objects.all()
        self.assertEqual(ticket.count(), 1)
        
        
        
    def test_add_Ticket_aGET(self):
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.addticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'add_Ticket.html')
        self.user.groups.remove(self.agroup)
        self.client.logout()

    def test_add_Ticket_aPOST(self):
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        self.client.post(self.addticket_url, {
            'title':'addTicket1',
            'severity':1,
            'description':'Testing adding tickets'
        })
        
        ticket = Ticket.objects.all()
        self.assertEqual(ticket.count(), 2)
        self.user.groups.remove(self.agroup)
        self.client.logout()
        
        
    def test_add_Ticket_dGET(self):
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.addticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'add_Ticket.html')
        self.user.groups.remove(self.dgroup)
        self.client.logout()

    def test_add_Ticket_dPOST(self):
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        self.client.post(self.addticket_url, {
            'title':'addTicket1',
            'severity':1,
            'description':'Testing adding tickets'
        })
        
        ticket = Ticket.objects.all()
        self.assertEqual(ticket.count(), 2)
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
        
    def test_add_Ticket_tGET(self):
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.addticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'add_Ticket.html')
        self.user.groups.remove(self.tgroup)
        self.client.logout()

    def test_add_Ticket_tPOST(self):
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        self.client.post(self.addticket_url, {
            'title':'addTicket1',
            'severity':1,
            'description':'Testing adding tickets'
        })
        
        ticket = Ticket.objects.all()
        self.assertEqual(ticket.count(), 2)
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
        
    def test_add_Ticket_bGET(self):
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.addticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'add_Ticket.html')
        self.user.groups.remove(self.bgroup)
        self.client.logout()

    def test_add_Ticket_bPOST(self):
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        self.client.post(self.addticket_url, {
            'title':'addTicket1',
            'severity':1,
            'description':'Testing adding tickets'
        })
        
        ticket = Ticket.objects.all()
        self.assertEqual(ticket.count(), 2)
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_add_Ticket_rGET(self):
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.addticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'add_Ticket.html')
        self.user.groups.remove(self.rgroup)
        self.client.logout()

    def test_add_Ticket_rPOST(self):
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        self.client.post(self.addticket_url, {
            'title':'addTicket1',
            'severity':1,
            'description':'Testing adding tickets'
        })
        
        ticket = Ticket.objects.all()
        self.assertEqual(ticket.count(), 2)
        self.user.groups.remove(self.rgroup)
        self.client.logout()
        
#####################viewticket test start##########################
        
    def test_viewticket_GET(self):
        #no login
        response = self.client.get(self.viewticket_url)
        self.assertNotEquals(response.status_code,200)
    
    
    
    def test_viewticket_aGET(self):        
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'view_Ticket.html')
        
        self.user.groups.remove(self.agroup)
        self.client.logout()
        
        
    def test_viewticket_dGET(self):        
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'view_Ticket.html')
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
        
    def test_viewticket_tGET(self):        
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'view_Ticket.html')
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
        
    def test_viewticket_bGET(self):        
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'view_Ticket.html')
        
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_viewticket_rGET(self):        
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'view_Ticket.html')
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()
        
########################ticketDetails test start##########################
        
        
    def test_ticketdetails_GET(self):
        # no login
        response = self.client.get(self.ticketdetails_url)
        self.assertNotEquals(response.status_code,200)
            
            
    def test_ticketdetails_aGET(self):        
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.ticketdetails_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'ticket_details.html')
        
        self.user.groups.remove(self.agroup)
        self.client.logout()
        
        
    def test_ticketdetails_dGET(self):        
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.ticketdetails_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'ticket_details.html')
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
        
    def test_ticketdetails_tGET(self):        
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.ticketdetails_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'ticket_details.html')
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
        
    def test_ticketdetails_bGET(self):        
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.ticketdetails_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'ticket_details.html')
        
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_ticketdetails_rGET(self):        
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.ticketdetails_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'ticket_details.html')
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()
        
#########################editTickets start##########################

    def test_editTickets_GET(self):
        #no login
        response = self.client.get(self.editTickets_url)
        self.assertNotEquals(response.status_code,200)
        
    def test_editTickets_POST(self):
        #no login
        response = self.client.post(self.editTickets_url, {
            'title':'Changed Ticket',
            'description':'Changed description',
            'status':'4'
        })
        self.assertNotEquals(response.status_code,200)
            
            
    def test_editTickets_aGET(self):        
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.editTickets_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'editTickets.html')
        
        self.user.groups.remove(self.agroup)
        self.client.logout()
        
    
    def test_editTickets_aPOST(self):
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.post(self.editTickets_url, {
            'title':'Changed Ticket',
            'description':'Changed description',
            'status':'4',
            'severity':'1',
            'validity':'1'
        })
        self.assertEquals(response.status_code,302)
        eticket = Ticket.objects.get(pk=self.ticket.pk)
        self.assertEquals(eticket.title, 'Changed Ticket')
        
        self.user.groups.remove(self.agroup)
        self.client.logout() 
        
        
    def test_editTickets_dGET(self):        
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.editTickets_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'editTickets.html')
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
    def test_editTickets_dPOST(self):
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.post(self.editTickets_url, {
            'title':'Changed Ticket',
            'description':'Changed description',
            'status':'4',
            'severity':'1',
            'validity':'1'
        })
        self.assertEquals(response.status_code,302)
        eticket = Ticket.objects.get(pk=self.ticket.pk)
        self.assertEquals(eticket.title, 'Changed Ticket') 
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()  
        
        
    def test_editTickets_tGET(self):        
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.editTickets_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'editTickets.html')
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
    def test_editTickets_tPOST(self):
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.post(self.editTickets_url, {
            'title':'Changed Ticket',
            'description':'Changed description',
            'status':'4',
            'severity':'1',
            'validity':'1'
        })
        self.assertEquals(response.status_code,302)
        eticket = Ticket.objects.get(pk=self.ticket.pk)
        self.assertEquals(eticket.title, 'Changed Ticket') 
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()  
        
        
    def test_editTickets_bGET(self):        
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.editTickets_url)
        self.assertEquals(response.status_code,200)
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_editTickets_bPOST(self):
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.post(self.editTickets_url, {
            'title':'Changed Ticket',
            'description':'Changed description',
            'status':'4',
            'severity':'1',
            'validity':'1'
        })
        eticket = Ticket.objects.get(pk=self.ticket.pk)
        self.assertNotEquals(eticket.title, 'Changed Ticket') 
        
        self.user.groups.remove(self.bgroup)
        self.client.logout()  
        
    def test_editTickets_rGET(self):        
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.editTickets_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'editTickets.html')
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()
    
    def test_editTickets_rPOST(self):
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.post(self.editTickets_url, {
            'title':'Changed Ticket',
            'description':'Changed description',
            'status':'4',
            'severity':'1',
            'validity':'1'
        })
        self.assertEquals(response.status_code,302)
        eticket = Ticket.objects.get(pk=self.ticket.pk)
        self.assertEquals(eticket.title, 'Changed Ticket') 
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()  
        
###############################viewprofile test start###################################      
          
          
    def test_viewprofile_GET(self):
        #no login
        response = self.client.get(self.viewprofile_url)
        self.assertNotEquals(response.status_code,200)
    
    
    
    def test_viewprofile_aGET(self):        
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewprofile_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myProfile.html')
        
        self.user.groups.remove(self.agroup)
        self.client.logout()
        
        
    def test_viewprofile_dGET(self):        
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewprofile_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myProfile.html')
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
        
    def test_viewprofile_tGET(self):        
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewprofile_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myProfile.html')
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
        
    def test_viewprofile_bGET(self):        
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewprofile_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myProfile.html')
        
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_viewprofile_rGET(self):        
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.viewprofile_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myProfile.html')
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()
        
        
###########################myTicket test start##########################
    
            
    def test_myticket_GET(self):
        #no login
        response = self.client.get(self.myticket_url)
        self.assertNotEquals(response.status_code,200)
    
    
    
    def test_myticket_aGET(self):        
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.myticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myTicket.html')
        
        self.user.groups.remove(self.agroup)
        self.client.logout()
        
        
    def test_myticket_dGET(self):        
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.myticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myTicket.html')
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
        
    def test_myticket_tGET(self):        
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.myticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myTicket.html')
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
        
    def test_myticket_bGET(self):        
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.myticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myTicket.html')
        
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_myticket_rGET(self):        
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.myticket_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'myTicket.html')
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()
        
        
#############################filterView test start###################################


        
    def test_filterview_GET(self):
        #no login
        response = self.client.get(self.filterview_url)
        self.assertNotEquals(response.status_code,200)
    
    
    
    def test_filterview_aGET(self):        
        #administrator
        self.user.groups.add(self.agroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.filterview_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'filterView.html')
        
        self.user.groups.remove(self.agroup)
        self.client.logout()
        
        
    def test_filterview_dGET(self):        
        #developer
        self.user.groups.add(self.dgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.filterview_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'filterView.html')
        
        self.user.groups.remove(self.dgroup)
        self.client.logout()
        
        
    def test_filterview_tGET(self):        
        #triager
        self.user.groups.add(self.tgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.filterview_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'filterView.html')
        
        self.user.groups.remove(self.tgroup)
        self.client.logout()
        
        
    def test_filterview_bGET(self):        
        #bug reporter
        self.user.groups.add(self.bgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.filterview_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'filterView.html')
        
        self.user.groups.remove(self.bgroup)
        self.client.logout()
        
        
    def test_filterview_rGET(self):        
        #reviewer
        self.user.groups.add(self.rgroup)
        self.client.login(username='testuser', password='testUser@123')
        response = self.client.get(self.filterview_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, 'filterView.html')
        
        self.user.groups.remove(self.rgroup)
        self.client.logout()
        