from django.http import request
from django.shortcuts import render , redirect
from django.template import loader
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .filters import TicketFilter
from .forms import editTicketsForm
from datetime import datetime
from dataclasses import dataclass, Field
from django import forms
from django.contrib.auth.models import User, Group
 
#import from models
from .models import *
from .forms import *
from .decorators import unauthenticated_user, allowed_users

# Create your views here.
@login_required(login_url='login')	#Only able to access when logged in
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
def indexView(request):
	page_title = "HDO - Home"
	context = {
		'page_title': page_title
	}
	return render(request, 'home.html', context)
	
@unauthenticated_user
def registerView(request):
	form = CreateUserForm()
	if request.method == 'POST':
		form = CreateUserForm(request.POST)
	if form.is_valid():
			form.save()
			user = form.cleaned_data.get('username')
			messages.success(request, 'Account was created for ' + user)
			
			return redirect ('login')
					
	context = {'form':form}
	return render(request, 'register.html', context)
	
@unauthenticated_user
def loginView(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
			
		user = authenticate(request, username=username, password=password)
			
		if user is not None:
			login(request, user)
			return redirect('home')
		else:
			messages.info(request, 'Username OR Password is incorrect')
			return redirect('login')
				
	return render(request, 'login.html')


def logoutView(request):
	logout(request)
	return redirect('login')

@unauthenticated_user
def forgetPassword(request):
	page_title = "HDO - Forget Password"
	error_message = ""
	if request.method == "POST":
		email = request.POST.get('email')
		if User.objects.filter(email=email).exists():
			uid = str(User.objects.get(email=email).id)
			return redirect('/change_password/' + uid)
		else:
			error_message = "No such user found"
			return render(request, 'pwReset.html', { 'page_title': page_title, 'error_message': error_message })
	return render(request, 'pwReset.html', { 'page_title': page_title, 'error_message': error_message })

@unauthenticated_user
def changePassword(request, pk):
	page_title = "HDO - Change Password"
	error_message = ""
	if request.method == "POST":
		if request.POST.get("pw1") == request.POST.get("pw2"):
			pwd = request.POST.get("pw1")
			user = User.objects.get(id=pk)
			user.set_password(pwd)
			user.save()
			return redirect('login')
		else:
			error_message = "Password does not match"
			return render(request, 'changePassword.html', { 'page_title': page_title, 'error_message': error_message })
	return render(request, 'changePassword.html', { 'uid': pk, 'page_title': page_title, 'error_message': error_message })

@login_required(login_url='login')	
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
def view_Ticket(request):
	
	# Tickets assigned to current logged in user
	page_title = 'HDO - View All Tickets'
	TicketTable = Ticket.objects.all()

	# Enum data from Tickets Table
	TicketStatus = dict(Ticket.STATUS)
	TicketSeverity = dict(Ticket.SEVERITY)
	TicketValidity = dict(Ticket.VALIDITY)
	
	#used to send information to our template
	context = {
			'page_title': page_title,
			'bugs': TicketTable,
			'ticket_status': TicketStatus,
			'ticket_severity': TicketSeverity,
			'ticket_validity': TicketValidity,
		}
	return render(request, 'view_Ticket.html', context)


# View my logged in user's ticket only
@login_required(login_url='login')	
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
def myTicket(request):
	# Page Title
	page_title = "HDO - My Tickets"

	# Current logged in user
	userNow = request.user
	userNowID = userNow.id
	
	# User table info
	userTable = User.objects.filter(id=userNowID)
	
	# Tickets assigned to current logged in user
	myTicket = Ticket.objects.filter(assignedTo=userNowID)

	# Enum data from Tickets Table
	TicketStatus = dict(Ticket.STATUS)
	TicketSeverity = dict(Ticket.SEVERITY)
	TicketValidity = dict(Ticket.VALIDITY)

	#used to send information to our template
	context = {
			'page_title': page_title,
			'user':userTable,
			'myTicket':myTicket,
			'ticket_status': TicketStatus,
			'ticket_severity': TicketSeverity,
			'ticket_validity': TicketValidity,
		}
	return render(request, 'myTicket.html', context)


# Edit tickets
@login_required(login_url='login')	#Only able to access when logged in
#@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Triager', 'Reviewer'])

def editTickets(request, pk):
	
	# edit tickets
	page_title = 'HDO - Edit ticket'
	# Get object of current ticket
	currentTicket = Ticket.objects.get(id=pk)
	# Get form for current ticket
	form = editTicketsForm(instance=currentTicket)
	
	current_user = request.user

	#if currentTicket.reportedBy != current_user and current_user.groups.filter(name = "Bug Reporter").exists():
	#	return redirect('/notAllowedToEdit')

	if request.method == 'POST':
		form = editTicketsForm(request.POST, instance=currentTicket)
		if form.is_valid():
			currentTicket.updated_at = datetime.now()
	
			if 'status' in form.changed_data:
				if currentTicket.status == '4':
					currentTicket.closed_at = datetime.now()
				else:
					currentTicket.closed_at = None
		
			form.save()

			return redirect('/ticket_Details/%s' % (pk))

	#used to send information to our template
	context = {
			'page_title': page_title,
			'currentTicket': currentTicket,
			'form' : form,
		}
	return render(request, 'editTickets.html', context)

def notAllowedToEdit(request):
	return render(request, 'notAllowedToEdit.html', {})

#filterview page
@login_required(login_url='login')	#Only able to access when logged in
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
def filterView(request):
	page_title = 'HDO - Filter View'
	tickets = Ticket.objects.all()
	myFilter = TicketFilter(request.GET, queryset=tickets)
	tickets = myFilter.qs
	status_text = dict(Ticket.STATUS)
	severity_text = dict(Ticket.SEVERITY)
	
	context = {
			'page_title': page_title,
			'tickets': tickets,
			'myFilter' : myFilter,
			'status_text': status_text,
			'severity_text': severity_text,
		}
	return render(request, "filterView.html", context)

@login_required(login_url='login')	#Only able to access when logged in
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
def add_Ticket(request):
	page_title = 'HDO - Add Ticket'
	form = TicketForm(request.POST)
	if request.method == 'POST':
		if form.is_valid():
			print ("Saving ticket")
			ticket = Ticket()
			ticket.title = form.cleaned_data['title']	
			ticket.description = form.cleaned_data['description']
			ticket.reportedBy = request.user
			ticket.save()
			return redirect('/ticket_Details/%s' % (ticket.id))
	return render(request, 'add_Ticket.html', {'page_title': page_title, 'form' : form})

@login_required(login_url='login')	#Only able to access when logged in
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
def ticket_details(request, pk):
	individualTicket = Ticket.objects.get(pk=pk)
	individualComments = Comments.objects.filter(ticket=individualTicket)
	individualTicket.status_text = dict(Ticket.STATUS)[individualTicket.status]
	individualTicket.severity_text = dict(Ticket.SEVERITY)[individualTicket.severity]
	individualTicket.validity_text = dict(Ticket.VALIDITY)[individualTicket.validity]

	form = CommentForm()
	if request.method == "POST" and "submitComment" in request.POST:
		form = CommentForm(request.POST)
		if form.is_valid():
			comment = Comments(
				comment=form.cleaned_data["content"],
				ticket=individualTicket,
				userID=request.user,
			)
			comment.save()
	if request.method == "POST" and "closeticket" in request.POST:
		#set ticket status to 'closed'
		individualTicket.status = '4'
		individualTicket.save()
		return redirect('viewticket')


	if individualTicket.status_text == 'Closed':
		remarks = "This ticket has been resolved and is closed"

	else:
		remarks = "We are resolving this bug at the moment."

	context = {
		'page_title': 'HDO - ' + individualTicket.title,
		"iT":individualTicket,
		"iC":individualComments,
		"form":form,
		"remarks":remarks
	}
	return render(request, "ticket_details.html", context)

@login_required(login_url='login')	#Only able to access when logged in
@allowed_users(allowed_roles=['Developer', 'Administrator', 'Bug Reporter', 'Triager', 'Reviewer'])
def viewProfile(request):
	page_title = "HDO - Profile"
	context = {
		'page_title': page_title
	}
	return render(request, 'myProfile.html', context)
